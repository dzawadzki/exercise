#include <iostream>
#include <vector>

#include "production.h"

int main(int argc, const char* argv[])
{
    auto params = getParams(argc, argv);
    for(const auto &i : params)
    {
        std::cout << i << std::endl;
    }
}

