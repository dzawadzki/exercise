#include <gtest/gtest.h>
#include <limits.h>

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

using namespace std;

TEST(testSuiteName, variableOverflowTest)
{
    int i = INT_MIN;
    int j = -i;
    j = j;

}

TEST(testSuiteName, arrayOutOfBoundsAccessTest)
{
    char *a = new char[10];
    a[10] = 1;
}

TEST(testSuiteName, memoryLeakTest)
{
    int *a = new int;
    a = a;
}

