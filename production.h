#pragma once

#include <vector>
#include <string>

std::vector<std::string> getParams(int argc, const char* argv[]);
std::vector<int> convertToInt(std::vector<std::string> strs);
std::string convertToRoman(int num);
